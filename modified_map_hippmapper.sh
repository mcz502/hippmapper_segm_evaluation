seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg"

for dir in $seg_dir/HP*; do
    cd $dir
    echo "Checking ${dir##*/}..."
    if [ -f ${dir##*/}_T1_brain_std_orient.nii.gz ]; then
       echo "${dir##*/}_T1_brain_std_orient.nii.gz exists...processing further..."
       
       mkdir -p $dir/map_segm  
       cd $dir/map_segm 
       
       if ! [ -f hipp_L_2Raw_masked.nii.gz ]; then 
          # Divide the segmentation output to left and right hemispheres 
          fslmaths ../${dir##*/}_hippmapper.nii.gz -thr 2 hipp_L
          fslmaths hipp_L -bin hipp_L_masked
          fslmaths ../${dir##*/}_hippmapper.nii.gz -uthr 1 hipp_R
       
          # Apply FLIRT with rigid body transformations to map them on original image
          flirt -in ../${dir##*/}_T1_brain_std_orient.nii.gz -ref ../${dir##*/}_T1_brain.nii.gz -omat tool2raw -out tool2raw.nii.gz -dof 6
          flirt -in hipp_L_masked.nii.gz -ref ../${dir##*/}_T1_brain.nii.gz -out hipp_L_2Raw -init tool2raw -applyxfm
          flirt -in hipp_R.nii.gz -ref ../${dir##*/}_T1_brain.nii.gz -out hipp_R_2Raw -init tool2raw -applyxfm
       
          # Binarize them to create masks for further comparison
          fslmaths hipp_L_2Raw.nii.gz -thr 0.5 -bin hipp_L_2Raw_masked
          fslmaths hipp_R_2Raw.nii.gz -thr 0.5 -bin hipp_R_2Raw_masked
          echo "${dir##*/}: Finished"
       else
          echo "${dir##*/}: outputs already exist..proceeding to next subject" 
       fi
        
    else
        mkdir -p $dir/map_segm  
        cd $dir/map_segm 
        echo "${dir##*/}_T1_brain_std_orient.nii.gz does not exist...skip FLIRT processing..."
        
        if ! [ -f  hipp_L_2Raw_masked.nii.gz ]; then
           # Divide the segmentation output to left and right hemispheres 
           fslmaths ../${dir##*/}_hippmapper.nii.gz -thr 2 hipp_L
           fslmaths hipp_L -bin hipp_L_masked
           fslmaths ../${dir##*/}_hippmapper.nii.gz -uthr 1 hipp_R

           # Binarize them to create masks for further comparison
           fslmaths hipp_L_masked.nii.gz -thr 0.5 -bin hipp_L_2Raw_masked
           fslmaths hipp_R.nii.gz -thr 0.5 -bin hipp_R_2Raw_masked
        
          echo "${dir##*/}: Finished"
        else
          echo "${dir##*/}: outputs already exist..proceeding to next subject"
        fi
    fi
done
