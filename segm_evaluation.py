import seg_metrics.seg_metrics as sg
import pandas as pd
import os

segm_dir = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg'
csv_file = os.path.join(segm_dir,'segm_evaluation.csv')
subjects=["HP01", "HP02", "HP03", "HP04", "HP05", "HP06", "HP07", "HP08", "HP09", "HP10", "HP11", "HP12", "HP14","HP15", "HP16", "HP17", "HP18", "HP19", "HP20", "HP21", "HP22", "HP23", "HP24", "HP25", "HP26","HP27", "HP28", "HP29", "HP30"]

hemisphere=['L', 'R']

segmentation_metrics = []

for subj in subjects:
    
    for h in hemisphere:
        
        # only binary masks
        labels = [1]
        
        # ground truth file
        gdth_file = os.path.join(segm_dir,subj,subj + '_' + h + 'hpc_mask.nii.gz') 
        
        # hippmapper files
        pred_file = os.path.join(segm_dir,subj,'map_segm','hipp_' + h + '_2Raw_masked.nii.gz')
                
        print(gdth_file)
        
        metrics = sg.write_metrics(labels, gdth_file, pred_file, metrics=['dice', 'hd', 'hd95','vs'])
        
        #Store metrics along with subject and hemisphere
        segmentation_metrics.append({
            'Subject': subj,
            'Hemisphere': h,
            'Metrics': metrics})

#Convoluted routine to get the values out of the segmentation metrics list/dictionary/list/dictionary 
all_metrics = []

# Loop through the segmentation_metrics list
for entry in segmentation_metrics:
    subject = entry['Subject']
    hemisphere = entry['Hemisphere']
    metrics_list = entry['Metrics']  
    
    # Loop through the metrics_list, which is a list containing a dictionary
    for metrics in metrics_list:
        dice_coefficient = metrics['dice']
        hausdorff_distance = metrics['hd']
        hd95 = metrics['hd95']
        volume_similarity = metrics['vs']
        
        # Create a list to represent the data for each metric entry
        metric_data = [subject, hemisphere, dice_coefficient, hausdorff_distance, hd95, volume_similarity]
        
        # Append the metric data to the all_metrics list
        all_metrics.append(metric_data)
        
# Create a DataFrame from the list of metrics
df = pd.DataFrame(all_metrics, columns=['Subject', 'Hemisphere', 'Dice Coefficient', 'Hausdorff Distance', 'HD95', 'Volume Similarity'])
        
# Save the DataFrame to a CSV file
df.to_csv(csv_file, index=False)