# hippmapper_segm_evaluation

## Useful scripts for evaluating HippMapp3r’s segmentation with ground truths

modified_map_hippmapper.sh: 
Use this to map hippmapper segmentations to the original image (FSL FLIRT rigid body); also separate the hippocampus segmentations into left and right hemispheres

segm_evaluation.py: 
Use this to evaluate the quality of segmentation. This will use seg-metrics package (https://pypi.org/project/seg-metrics/)

Additional methods: This folder contains scripts for mapping FreeSurfer and CAT12 hippocampus segmentations on ground truth images. CAT12 method had 9 atlases. The atlas labels are also provided.