#seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/freesurfer_hippocampus_outputs"
seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/freesurfer_hippocampus_outputs"

for dir in $seg_dir/HP*; do
    
    echo "Processing ${dir##*/}..."      
    cd $dir

    if ! [ -f hipp_R_2Raw_masked.nii.gz ]; then 

       # Divide the segmentation output to left and right hemispheres 
       mri_binarize --i aseg.mgz --match 17 --o lh_hipp.mgz
       mri_binarize --i aseg.mgz --match 53 --o rh_hipp.mgz

       # Convert them to nii.gz images
       mri_convert lh_hipp.mgz lh_hipp.nii.gz
       mri_convert rh_hipp.mgz rh_hipp.nii.gz

       # Convert brain extracted image from freesurfer (mgz)to nii.gz
       mri_convert brainmask.mgz T1_brain.nii.gz
       
       # Apply FLIRT with rigid body transformations to map them on original image
       flirt -in T1_brain.nii.gz -ref ../../${dir##*/}/${dir##*/}_T1_brain.nii.gz -omat tool2raw -out tool2raw.nii.gz -dof 6 -interp nearestneighbour
       flirt -in lh_hipp.nii.gz -ref ../../${dir##*/}/${dir##*/}_T1_brain.nii.gz -out hipp_L_2Raw -init tool2raw -applyxfm -interp nearestneighbour
       flirt -in rh_hipp.nii.gz -ref ../../${dir##*/}/${dir##*/}_T1_brain.nii.gz -out hipp_R_2Raw -init tool2raw -applyxfm -interp nearestneighbour
       
       # Binarize them to create masks for further comparison
       fslmaths hipp_L_2Raw.nii.gz -thr 0.5 -bin hipp_L_2Raw_masked
       fslmaths hipp_R_2Raw.nii.gz -thr 0.5 -bin hipp_R_2Raw_masked
       echo "${dir##*/}: Finished"
    else
       echo "${dir##*/}: outputs already exist..proceeding to next subject" 
    fi        
done
