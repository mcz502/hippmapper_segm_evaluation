seg_dir="/vols/Data/pipelines-DPUK/BHC_cat/segm_eval_work/julichbrain"

for dir in $seg_dir/HP*; do
    
    echo "Processing ${dir##*/}..."      
    cd $dir

    if ! [ -f hipp_R_2Raw_masked.nii.gz ]; then 
       # create the brain extracted image for registration later
       fslmaths p0T1_orig_defaced.nii -bin brain_mask.nii
       fslmaths brain_mask.nii.gz -mul T1_orig_defaced.nii.gz T1_brain
 	
       # Divide the segmentation output to left and right hemispheres 
       mri_binarize --i julichbrain_T1_orig_defaced.nii --match 5 43 96 101 111 118 125 133 173 181 --o left_hipp.nii.gz
       mri_binarize --i julichbrain_T1_orig_defaced.nii --match 6 44 97 102 114 119 126 134 174 184 --o right_hipp.nii.gz

       # Apply FLIRT with rigid body transformations to map them on original image
       flirt -in T1_brain.nii.gz -ref ${dir##*/}_T1_brain.nii.gz -omat tool2raw -out tool2raw.nii.gz -dof 6 -interp nearestneighbour
       flirt -in left_hipp.nii.gz -ref ${dir##*/}_T1_brain.nii.gz -out hipp_L_2Raw -init tool2raw -applyxfm -interp nearestneighbour
       flirt -in right_hipp.nii.gz -ref ${dir##*/}_T1_brain.nii.gz -out hipp_R_2Raw -init tool2raw -applyxfm -interp nearestneighbour
       
       # Binarize them to create masks for further comparison
       fslmaths hipp_L_2Raw.nii.gz -thr 0.5 -bin hipp_L_2Raw_masked
       fslmaths hipp_R_2Raw.nii.gz -thr 0.5 -bin hipp_R_2Raw_masked
       echo "${dir##*/}: Finished"
    else
       echo "${dir##*/}: outputs already exist..proceeding to next subject" 
    fi        
done
